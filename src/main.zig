const std = @import("std");
const print = std.debug.print;
const assert = std.debug.assert;
const log = std.log;

const specs = @import("specs.zig");
const Manager = @import("Manager.zig");

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer assert(!gpa.deinit());

    const allocator = gpa.allocator();

    var spec = try specs.Spec.init(allocator, &.{.all}, "/home/haoliang/.local/share/nvim/viz", &.{
        .{ .profile = .base, .uri = "https://github.com/lewis6991/impatient.nvim" },
        .{ .profile = .base, .uri = "https://github.com/tpope/vim-repeat" },
        .{ .profile = .base, .uri = "https://github.com/junegunn/vim-easy-align" },
        .{ .profile = .base, .uri = "https://github.com/michaeljsmith/vim-indent-object" },
        .{ .profile = .base, .uri = "https://github.com/tpope/vim-surround" },
        .{ .profile = .base, .uri = "https://github.com/haolian9/hop.nvim" },

        .{ .profile = .joy, .uri = "https://github.com/haolian9/guwen.nvim" },

        .{ .profile = .lsp, .uri = "https://github.com/neovim/nvim-lspconfig" },
        .{ .profile = .lsp, .uri = "https://github.com/nvim-lua/plenary.nvim" },
        // .{ .profile = .lsp, .uri = "https://github.com/haolian9/null-ls.nvim", .branch = "hal", .as = "null-ls-hal" },
        .{ .profile = .lsp, .uri = "/srv/playground/null-ls.nvim" },

        .{ .profile = .treesitter, .uri = "https://github.com/nvim-treesitter/nvim-treesitter" },
        //.{ .profile = .treesitter, .uri = "https://github.com/haolian9/nvim-treesitter", .branch = "hal", .as = "nvim-treesitter-hal" },
        //.{ .profile = .treesitter, .uri = "https://github.com/nvim-treesitter/nvim-treesitter-refactor" },
        .{ .profile = .treesitter, .uri = "https://github.com/haolian9/nvim-treesitter-refactor", .branch = "hal", .as = "nvim-treesitter-refactor-hal" },
        //.{ .profile = .treesitter, .uri = "https://github.com/nvim-treesitter/playground" },

        .{ .profile = .code, .uri = "https://github.com/SirVer/ultisnips" },
        .{ .profile = .code, .uri = "https://github.com/tpope/vim-commentary" },

        .{ .profile = .git, .uri = "https://github.com/tpope/vim-fugitive" },
        .{ .profile = .git, .uri = "https://github.com/junegunn/gv.vim" },

        .{ .profile = .lua , .uri = "https://github.com/haolian9/emmylua-stubs" },
    });
    defer spec.deinit();

    {
        var manager = Manager{ .allocator = allocator, .spec = spec };
        // try manager.install();
        try manager.update();
        // try manager.collapse("/tmp/viz-collapsed");
    }

    {
        var rtp = try specs.VimRtp.fromSpec(allocator, spec);
        defer rtp.deinit();

        if (false) {
            try rtp.dump(std.io.getStdOut().writer());
        } else {
            const home = std.os.getenv("HOME") orelse return error.NoHome;
            const output_path = try std.fs.path.join(allocator, &.{ home, ".local/share/nvim", "viz.json" });
            defer allocator.free(output_path);
            var file = try std.fs.createFileAbsolute(output_path, .{});
            defer file.close();
            try rtp.dump(file.writer());
            log.info("dumped rtp as {s}", .{output_path});
        }
    }
}
