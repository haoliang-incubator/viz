// todo: too much apis to learn!
// ref: https://libgit2.org/docs/guides/101-samples/

const std = @import("std");
const print = std.debug.print;
const assert = std.debug.assert;

const c = @cImport(@cInclude("git2.h"));

// ref: https://libgit2.org/docs/examples/init/
fn demoInit() void {
    // var repo: c.git_repository = undefined;
    // const rc = c.git_repository_init(&repo, "/tmp/viz.git", false);
    // print("rc={d}\n", .{rc});
}

pub fn main() !void {
    print("hello world!\n", .{});
    print("{any}", .{c.GIT_STATUS_CURRENT});
}

// asyncrun: zig run -lc -lgit2
