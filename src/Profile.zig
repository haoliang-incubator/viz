const std = @import("std");
const print = std.debug.print;
const assert = std.debug.assert;

// todo: possible alternatives: std.enums.EnumSet, EnumFieldStruct
// todo: possible alternatives: packed struct { r: bool, w: bool, x: bool }

fn GetEnum(comptime someEnum: type) type {
    var fields: []const std.builtin.Type.EnumField = std.meta.fields(someEnum);
    var counter = fields.len;
    inline for (std.meta.fields(someEnum)) |x, i| {
        inline for (std.meta.fields(someEnum)[i + 1 ..]) |y| {
            fields = fields ++ @as([]const std.builtin.Type.EnumField, &.{std.builtin.Type.EnumField{ .name = x.name ++ y.name, .value = counter }});
            counter += 1;
        }
    }
    var enumInfo = @typeInfo(someEnum);
    return @Type(.{ .Enum = std.builtin.Type.Enum{
        .layout = enumInfo.Enum.layout,
        .tag_type = std.math.IntFittingRange(0, fields.len),
        .fields = fields,
        .decls = &.{},
        .is_exhaustive = true,
    } });
}

const Mode = GetEnum(enum { x, r, w });

pub fn main() !void {

    // std.enums.EnumSet

    print("{d} {d}", .{ @enumToInt(Mode.x), @enumToInt(Mode.rw) });
}
