const std = @import("std");

pub fn build(b: *std.build.Builder) void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions();
    const strip = mode != .Debug;

    const exe = b.addExecutable("viz", "src/main.zig");
    exe.setTarget(target);
    exe.strip = strip;
    exe.setBuildMode(mode);
    exe.install();

    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(blk: {
        const tests = b.addTest("src/main.zig");
        break :blk &tests.step;
    });
}
